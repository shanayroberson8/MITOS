2017-06-16 (6b33f95) | added missing column in gff output, switch to git (<a href="https://gitlab.com/Bernt/MITOS" target="_blank">git</a>)
2017-02-08 (revision 942) | bugfix tRNA anticodon was actually the codon in table, bed, and gff  
2015-03-31 (revision 656) | Added the JobID to the email header for easier identification in the inbox 
2015-01-02 (revision 605) | Allow multi fasta input
2014-06-18 (revision 526) | Added FAA output and anticodon position in the result file contained in the raw data. 
2014-04-28 (revision 481) | Bugfix: technical error in clipping 
2014-01-23 (revision 448) | Bugfix: TBL (formerly called sequin) output was 0 based 
2013-09-29 (revision 364) | Added "Status and Statistics" page. | Small bugfixes
2013-05-28 (revision 342) | Added job settings page &amp; legend of the figure indicates what MITOS was searching for | Added ncRNA plot &amp; improved protein plot | Determine and list missing, duplicated, or non-standard genes | Jobs with deleted results can be restarted 
2013-03-12 (revision 293) | Bugfix: "Cutoff" was parsed such that values smaller X (instead of X% of the maximum) were discarded 
2013-03-06 (revision 291) | In order to prevent errors due to line breaks in the MITOS results email MITOS uses now much shorter job IDs. | Improved error messages in case of invalid access to the results page

2013-02-10 (revision 281) | Bugfix: "Fragment Overlap" and "Maximum Overlap" have been passed to MITOS 100x to large (as a consequence arbitrary overlaps of protein coding genes were possible) | Empty FASTA header caused a bug in MITOS (now set to NoName)
2013-01-17 (revision 272) | Bugfix: start and end improvement for proteins was called twice for features consisting of a single part 
2013-01-03 (revision 259) | Bugfix: Identification of parts did not work (all parts have been identified as different copies). | MITOS revision is shown on the bottom with link to the changelog. 
2012-12-15 (revision 247) | Invalid fasta files give a suitable error message.
2012-12-07 (revision 240) | Unicode in names and file names is now correctly handled.
2012-11-29 (revision 236) | Bugfix: glocal search mode was also executed for tRNAs (which slowed MITOS down).    
2012-11-27 (revision 234) | Bugfix: rRNAs were reported with wrong name (trn12s and trn16s).
